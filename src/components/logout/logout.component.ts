import { Component } from '@angular/core';
import { LoginComponent } from "../../pages/login/login.component";
import { NavController } from "ionic-angular/index";

@Component({
  selector: 'btn-logout',
  template: '<button ion-item small (click)="logout()">Logout</button>'
})
export class LogoutComponent {

  constructor(private navController: NavController) {}

  logout() {
    localStorage.removeItem('token');
    this.navController.push(LoginComponent);
  }
}
