import { Component, ViewChild } from '@angular/core';
import { Nav } from 'ionic-angular';

import { HomePage } from '../../pages/home/home';
import { AnnouncementsPage } from '../../pages/announcements/announcements';
import { ReportsPage } from '../../pages/reports/reports';
import { DevicesPage } from '../../pages/devices/devices';
import { ChangepasswordPage } from '../../pages/changepassword/changepassword';


@Component({
  templateUrl: 'menu.html'
})
export class MenuComponent {
  @ViewChild('content') nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor() {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Announcements', component: AnnouncementsPage },
      { title: 'Reports', component: ReportsPage },
      { title: 'Location Statuses', component: DevicesPage },
      { title: 'Change Password', component: ChangepasswordPage },
    ];
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}