import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NavController,  AlertController } from 'ionic-angular';

@Injectable()
export class LoginServiceProvider {
  data: any;
  constructor(public http: Http, public alertCtrl: AlertController, public navCtrl: NavController) {
  }

  login(params) {
    this.http.post('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/authenticate', params)
      .map(res => res.json())
      .subscribe( result => {
        if (result.token != '' || result.token != null) {
          localStorage.setItem('token', result.token);
          this.navCtrl.pop();
          console.log('token', result.token);
        } else {
          let alert = this.alertCtrl.create({
            title: 'Login Failed',
            subTitle: 'Please check your credentials.',
            buttons: ['Dismiss'],
          });
          alert.present();
        }
      }, error => {
        this.data = JSON.parse(error.text());
        let alert = this.alertCtrl.create({
            title: 'Login Failed',
            subTitle: this.data.error,
            buttons: ['Dismiss'],
          });
          alert.present();
      });
  }
}
