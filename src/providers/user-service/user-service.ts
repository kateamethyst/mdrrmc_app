import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController,  AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class UserServiceProvider {
  data : any;
  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public http: Http) {
  }

  /**
   * Register new user
   * @param user object
   */
  register(params) {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.post('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/users/register', params)
      .subscribe(data => {
          this.data = data;
          let alert = this.alertCtrl.create({
            title: 'Successfully Reistered',
            subTitle: 'Please check your email. Thank you',
            buttons: [
              {
                text: 'OK',
                role: 'ok',
                handler: () => {
                  this.navCtrl.pop();
                }
              },
            ]
          });
          alert.present();
      }, error => {
        let alert = this.alertCtrl.create({
          title: 'Registration Failed',
          subTitle: 'Email is already registered.',
          buttons: ['OK'],
        });
        alert.present();
      });
    });
  }

  /**
   * Get profile
   */
  getProfile() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      let headers = new Headers();
      let token = localStorage.getItem('token');
      headers.append('Authorization', 'Bearer ' + token);
      this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/me', {headers: headers} )
          .subscribe(data => {
            this.data = JSON.parse(data.text());
            localStorage.setItem('id', this.data.id);
            resolve(this.data);
          },error => {
            let alert = this.alertCtrl.create({
              title: 'Session Expired',
              subTitle: 'Please login again. Thank you!',
              buttons: [
              {
                text: 'Dismiss',
                role: 'dismiss',
                handler: () => {
                  this.navCtrl.pop();
                }
              },
            ]
            });
            alert.present();
          });
    });
  }

  /**
   * Change Password
   * @param object old password and password
   */
  changePassword(params) {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      let headers = new Headers();
      let token = localStorage.getItem('token');
      let user_id = localStorage.getItem('id');
      headers.append('Authorization', 'Bearer ' + token);
      this.http.put('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/users/change-password/' + user_id , params, {headers: headers} )
          .subscribe(data => {
            resolve(this.data);
            let alert = this.alertCtrl.create({
              title: 'Message',
              subTitle: 'Successfully updated password',
              buttons: ['OK']
            });
            alert.present();
          },error => {
            let alert = this.alertCtrl.create({
              title: 'Incorrect Password',
              subTitle: 'Please try again. Thank you!',
              buttons: ['Dismiss'],
            });
            alert.present();
          });
    });
  }
}
