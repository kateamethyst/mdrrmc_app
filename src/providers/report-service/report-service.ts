import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavController,  AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Injectable()
export class ReportServiceProvider {
  data: any;
  constructor(public http: Http, public alertCtrl: AlertController, public navCtrl: NavController) {
  }

  /**
   * Fecth articles
   */
  load() {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/articles/approved')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }


  /**
   * Fetch recent
   */
  recent() {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/articles/recent')
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        });
    });
  }

  store(params) {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      let headers = new Headers();
      let token = localStorage.getItem('token');
      headers.append('Authorization', 'Bearer ' + token);
      headers.append('Content-Type', 'application/json;charset=UTF-8');
      this.http.post('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/articles', params, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           let alert = this.alertCtrl.create({
            title: 'Successfully send',
            subTitle: 'Thank you for reporting! We will review your report.',
            buttons: [
              {
                text: 'Dismiss',
                role: 'dismiss',
                handler: () => {
                  this.navCtrl.pop();
                }
              },
            ]
          });

            alert.present();
           resolve(this.data);
        }, error => {
           throw error;
         });
    });
  }
}