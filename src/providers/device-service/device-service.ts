import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NavController,  AlertController } from 'ionic-angular';

@Injectable()
export class DeviceServiceProvider {
  data: any;
  constructor(public http: Http, public alertCtrl: AlertController, public navCtrl: NavController) {
  }

  allDeviceReport() {
      if (this.data) {
        return Promise.resolve(this.data);
      }
      return new Promise(resolve => {
        this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports/top')
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data);
          });
      });
  }
}
