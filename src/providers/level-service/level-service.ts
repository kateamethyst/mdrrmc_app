import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LevelServiceProvider {
  data : any;
  constructor(public http: Http) {
  }

  /**
   * Fecth Announcement
   */
  load(device : any) {
      if (this.data) {
        return Promise.resolve(this.data);
      }
      return new Promise(resolve => {
        this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?limit=1&device_id=' + device)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data.result;
            if (this.data.level <= this.data.yellow_warning && this.data.level >= 0) {
              this.data.warning = 'Yellow';
              this.data.message = 'Mag antabay sa mga babala.';
            } else if (this.data.level > this.data.yellow_warning && this.data.level <= this.data.orange_warning && this.data.level <= this.data.red_warning) {
              this.data.warning = 'Orange';
              this.data.message = 'Alerto sa posibleng paglikas.';
            } else if (this.data.level > this.data.orange_warning && this.data.level <= this.data.red_warning || this.data.level >= this.data.red_warning) {
              this.data.warning = 'Red';
              this.data.message = 'Lumikas sa pinakamalhttp://mdrrmo.ap-southeast-1.elasticbeanstalk.com/apit na evacuation center.';
            } else {
              this.data.warning = '';
              this.data.message = '';
            }
            resolve(this.data);
          });
      });
  }

  topLevel(device : any) {
      if (this.data) {
        return Promise.resolve(this.data);
      }
      return new Promise(resolve => {
        this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?limit=5&offset=1&device_id=' + device)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data.result;
            resolve(this.data);
          });
      });
  }

}
