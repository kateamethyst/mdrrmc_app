import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AnnouncementServiceProvider {
  data : any;
  constructor(public http: Http) {
  }

  /**
   * Fecth Announcement
   */
  load() {
      if (this.data) {
        return Promise.resolve(this.data);
      }
      return new Promise(resolve => {
        this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/announcements')
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data);
          });
      });
  }

  /**
   * Fetch recent
   */
  recent() {
    if (this.data) {
      return Promise.resolve(this.data);
    }
    return new Promise(resolve => {
      this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/announcements/recent')
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        });
    });
  }
}
