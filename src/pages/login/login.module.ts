import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { LoginComponent } from "./login.component";
import { LogoutComponent } from "../../components/logout/logout.component";

@NgModule({
  imports: [IonicModule],
  declarations: [LoginComponent, LogoutComponent],
  entryComponents: [LoginComponent, LogoutComponent],
  exports: [LogoutComponent]
})
export class LoginModule {}
