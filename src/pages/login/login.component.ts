import { Component } from '@angular/core';
import { NavController,  AlertController } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  templateUrl: 'login.html',
  providers: [],
})
export class LoginComponent {
  registrationPage = RegistrationPage;
  data: any;
  disabled: any;
  button_text: any;

   constructor(public http: Http, public alertCtrl: AlertController, public navCtrl: NavController) {
    this.data = {
      email: '',
      password: '',
    };
    this.button_text = 'LOG IN';
  }

  loginAsGuest() {
    this.navCtrl.pop();
  }

  authenticate() {
    this.disabled = true;
    this.button_text = 'PLEASE WAIT';
    let params = this.data;
    localStorage.setItem('token', '');
    this.http.post('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/authenticate', params)
      .map(res => res.json())
      .subscribe( result => {
        if (result.token != '' || result.token != null) {
          localStorage.setItem('token', result.token);
          this.navCtrl.pop();
        } else {
          let alert = this.alertCtrl.create({
            title: 'Login Failed',
            subTitle: 'Please check your credentials.',
            buttons: ['Dismiss'],
          });
          alert.present();
        }
      }, error => {
        this.data = JSON.parse(error.text());
        this.disabled = false;
        this.button_text = 'LOG IN';
        let alert = this.alertCtrl.create({
            title: 'Login Failed',
            subTitle: this.data.error,
            buttons: ['Dismiss'],
          });
          alert.present();
      });
  }
}
