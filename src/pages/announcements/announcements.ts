import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AnnouncementServiceProvider } from '../../providers/announcement-service/announcement-service';
import { AnnouncementPage } from './announcement/announcement';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the AnnouncementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-announcements',
  templateUrl: 'announcements.html',
  providers: [AnnouncementServiceProvider],
})
export class AnnouncementsPage {

  public announcements: any;
  constructor(public http: Http, public navCtrl: NavController, public announcementSearch: AnnouncementServiceProvider) {
      this.loadAnnouncement();
  }

  doRefresh(refresher) {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/announcements')
      .map(res => res.json())
      .subscribe(data => {
        this.announcements = data;
        refresher.complete();
      });
  }


  loadAnnouncement() {
    this.announcementSearch.load()
      .then(data => {
        this.announcements = data;
        console.log(data);
      });
  }

  onLoadAnnouncement(announcement: any) {
    this.navCtrl.push(AnnouncementPage, {announcement: announcement});
  }
}
