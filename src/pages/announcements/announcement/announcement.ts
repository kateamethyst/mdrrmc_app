import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';
/**
 * Generated class for the AnnouncementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-announcement',
  templateUrl: 'announcement.html',
})
export class AnnouncementPage implements OnInit{

  public announcement: any;
  constructor(private navParams: NavParams) {
  }

  ngOnInit() {
    this.announcement = this.navParams.get('announcement');
  }

}
