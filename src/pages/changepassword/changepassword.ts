import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { HomePage } from '../../pages/home/home';
/**
 * Generated class for the ChangepasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
  providers: [UserServiceProvider],
})
export class ChangepasswordPage {

  public setup:any
  changePassword: FormGroup;
  constructor(public user: UserServiceProvider, public formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
      this.setup = {
        old_password: '',
        new_password: '',
        confirm_password: '',
      };

      this.changePassword = formBuilder.group({
        old_password: ['', Validators.compose([Validators.required])],
        new_password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,12}$')])],
        confirm_password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      });
  }

  change() {
    let params = this.setup;
    this.user.changePassword(params).then(result => {
      this.setup = {
        old_password: '',
        new_password: '',
        confirm_password: '',
      };
      this.navCtrl.setRoot(HomePage);
    });
  }
}

