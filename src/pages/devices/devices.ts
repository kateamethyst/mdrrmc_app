import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DeviceServiceProvider } from '../../providers/device-service/device-service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the AnnouncementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html',
  providers: [DeviceServiceProvider],
})
export class DevicesPage {

  public devices: any;
  constructor(public http: Http, public navCtrl: NavController, public deviceSearch: DeviceServiceProvider) {
      this.loadAnnouncement();
  }

  doRefresh(refresher) {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports/top')
      .map(res => res.json())
      .subscribe(data => {
        this.devices = data;
        refresher.complete();
      });
  }


  loadAnnouncement() {
    this.deviceSearch.allDeviceReport()
      .then(data => {
        this.devices = data;
        console.log('load', data);
      });
  }
}
