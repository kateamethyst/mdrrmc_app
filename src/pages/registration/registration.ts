import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';

/**
 * Generated class for the RegistrationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
  providers: [UserServiceProvider],
})
export class RegistrationPage {
  inquiry: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public user: UserServiceProvider) {
    this.inquiry = {
      first_name: '',
      last_name: '',
      contact_number: '',
      email: '',
      role: 'resident',
    };
  }

  registerUser() {
    let params = this.inquiry;
    this.user.register(params)
      .then(data => {
      });
    }
}
