import { Component } from '@angular/core';
import { NavController, ToastController  } from 'ionic-angular';
import { AnnouncementServiceProvider } from '../../providers/announcement-service/announcement-service';
import { ReportServiceProvider } from '../../providers/report-service/report-service';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LevelServiceProvider } from '../../providers/level-service/level-service';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [LevelServiceProvider, AnnouncementServiceProvider, ReportServiceProvider, UserServiceProvider],
})
export class HomePage {

  public announcements: any;
  public reports: any;
  public profile: any;
  public level: any;
  public tops: any;
  public devices: any;
  public location: any;

  constructor(private iab: InAppBrowser, public toastCtrl: ToastController, public http: Http, public levelService: LevelServiceProvider, public navCtrl: NavController, public recentAnnouncement: AnnouncementServiceProvider, public reportRecent: ReportServiceProvider, public user: UserServiceProvider) {
    this.level = {
      location: '',
      level: '',
      date: '',
      id: '',
      warning: '',
      message: '',
    }

    this.profile = {
      first_name : '',
    };

    this.location = localStorage.getItem('device_id');
    if (this.location != null) {
      this.loadLevel();
    } else {
      this.loadProfile();
      this.loadLevel();
    }

    this.loadRecentAnnouncement();
    this.loadRecentReports();
    this.loadDevices();
    this.loadTopReport();
  }

  openTides() {
    const browser = this.iab.create('http://myforecast.co/bin/tide.m?city=72336&metric=false&tideLocationID=T1331');
    browser.show();
  }

  doRefresh(refresher : any) {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/articles/approved')
      .map(res => res.json())
      .subscribe(data => {
        this.reports = data;
        refresher.complete();
      });

    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/announcements')
      .map(res => res.json())
      .subscribe(data => {
        this.announcements = data;
        refresher.complete();
      });

      this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water-level-devices')
      .map(res => res.json())
      .subscribe(data => {
        this.devices = data;
        refresher.complete();
      });

    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?device_id=' + this.location + '&limit=1')
          .map(res => res.json())
          .subscribe(data => {
            this.level = data.result[0];
            console.log('limit 1', this.level);
            this.level = this._checkWarning(this.level);
          refresher.complete();
          });

    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?device_id=' + this.location + '&limit=5&offset=1')
      .map(res => res.json())
      .subscribe(data => {
        this.tops = data.result;
        refresher.complete();
      });
  }

  loadDevices() {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water-level-devices')
      .map(res => res.json())
      .subscribe(data => {
        this.devices = data;
      });
  }

  loadRecentAnnouncement() {
    this.recentAnnouncement.recent()
      .then(data => {
        this.announcements = data;
      });
  }

  loadTopReport() {
    this.levelService.topLevel(this.location)
      .then(data => {
        this.tops = data;

    console.log('top', this.tops);
      });
  }

  loadLevel() {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?limit=1&device_id=' + this.location)
      .map(res => res.json())
      .subscribe(data => {
        this.level = data.result[0];
        console.log('update', data);
        this.level = this._checkWarning(this.level);
      });
  }

  loadRecentReports() {
    this.reportRecent.recent()
      .then(data => {
        this.reports = data;
      });
  }

  loadProfile() {
    this.user.getProfile()
      .then(data => {
        this.profile = data;
        if (data['device_id'] == null) {
          this.location = 1;
        } else {
          this.location = data['device_id'];
        }
        localStorage.setItem('device_id', this.location);
      });
  }

  updateLocation(selectedValue: any){
    let params = {
      id: this.profile['id'],
      device_id: selectedValue
    };
    this.http.post('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/users/update-location', params)
        .map(res => res.json())
        .subscribe(data => {
          this.location = selectedValue;
          localStorage.setItem('device_id', selectedValue);
          let toast = this.toastCtrl.create({
            message: 'Location successfully saved. Please wait change to take effect.',
            duration: 3000
          });
          toast.present();
        }, error => {
          this.location = 1;
           throw error;
        });

    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?limit=1&device_id=' + selectedValue)
      .map(res => res.json())
      .subscribe(data => {
        this.level = data.result[0];
        console.log('update', data);
        this.level = this._checkWarning(this.level);
      });

    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/water_level_reports?device_id=' + selectedValue + '&limit=5&offset=1')
      .map(res => res.json())
      .subscribe(data => {
        this.tops = data.result;
      });
  }

  _checkWarning(level) {
    if (level.warning === 'yellow') {
      level.warning = 'Yellow';
      level.message = 'Mag antabay sa mga babala.';
    } else if (level.warning === 'orange') {
      level.warning = 'Orange';
      level.message = 'Alerto sa posibleng paglikas.';
    } else if (level.warning === 'red') {
      level.warning = 'Red';
      level.message = 'Lumikas sa pinakamalapit na evacuation center.';
    } else {
      this.level.warning = 'Blue';
      this.level.message = '';
    }

    return level;
  }
}
