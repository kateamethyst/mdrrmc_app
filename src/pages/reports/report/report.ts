import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';
/**
 * Generated class for the AnnouncementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage implements OnInit{

  public report: any;
  constructor(private navParams: NavParams) {
  }

  ngOnInit() {
    this.report = this.navParams.get('report');
  }

}
