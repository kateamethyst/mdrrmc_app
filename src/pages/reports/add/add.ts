import { Component } from '@angular/core';
import { ReportServiceProvider } from '../../../providers/report-service/report-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AnnouncementsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
  providers: [ReportServiceProvider],
})
export class AddPage{

  public report: any;
  request: FormGroup;
  constructor(public formBuilder: FormBuilder, public reports: ReportServiceProvider) {
    this.report = {
      title: '',
      description: '',
      contact_number: '',
      address: '',
      image: '',
      is_approved: 2
    };
    this.request = formBuilder.group({
      title : ['', Validators.compose([Validators.required, Validators.minLength(4)], )],
      description:  ['', Validators.compose([Validators.minLength(8), Validators.required]), ],
      address:  ['', Validators.compose([Validators.minLength(6), Validators.required])],
      contact_number:  ['', Validators.compose([Validators.minLength(6), Validators.required])],
    });
  }


  add() {
    let params = this.report;
    this.reports.store(params)
      .then(data => {
      });
  }
}
