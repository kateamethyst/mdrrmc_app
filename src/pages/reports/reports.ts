import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReportServiceProvider } from '../../providers/report-service/report-service';
import { ReportPage } from './report/report';
import { AddPage } from './add/add';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
  providers: [ReportServiceProvider],
})
export class ReportsPage {

  public reports: any;
  constructor(public http: Http, public navCtrl: NavController, public reportSearch: ReportServiceProvider) {
      this.loadReports();
  }

  doRefresh(refresher) {
    this.http.get('http://mdrrmo.ap-southeast-1.elasticbeanstalk.com/api/articles/approved')
      .map(res => res.json())
      .subscribe(data => {
        this.reports = data;
        refresher.complete();
      });
  }

  loadReports() {
    this.reportSearch.load()
      .then(data => {
        this.reports = data;
        console.log(data);
      });
  }

  onLoadReport(report: any) {
    this.navCtrl.push(ReportPage, {report: report});
  }

  loadAddPage() {
    this.navCtrl.push(AddPage)
  }
}
