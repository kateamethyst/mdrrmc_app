import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MenuComponent } from "../components/menu/menu.component";
import { LoginServiceProvider } from '../providers/login-service/login-service';
import { LoginComponent } from "../pages/login/login.component";

@Component({
  template: '<ion-nav #baseNav></ion-nav>',
  providers: [LoginServiceProvider]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.platform.ready().then(() => {
        this.platform.registerBackButtonAction(() => {
            let activeVC = this.nav.getActive();
            let page = activeVC.instance;
            if((page instanceof LoginComponent)){
              navigator['app'].exitApp();
            } else {
              this.nav.pop();
            }
        });
    });
  }

  ngOnInit() {
    const componentStack: Array<{page: Component}> = [{
      page: MenuComponent
    }];

    // if (!this.loginService.isLoggedIn) {
      componentStack.push({ page: LoginComponent });
    // }

    this.nav.insertPages(0, componentStack, { animate: false });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
