import { BrowserModule }                            from '@angular/platform-browser';
import { ErrorHandler, NgModule }                   from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { HttpModule }                  from '@angular/http';
import { MyApp }                       from './app.component';
import { HomePage }                    from '../pages/home/home';
import { AnnouncementsPage }           from '../pages/announcements/announcements';
import { ReportsPage }                 from '../pages/reports/reports';
import { RegistrationPage }            from '../pages/registration/registration';
import { AnnouncementPage }            from '../pages/announcements/announcement/announcement';
import { DevicesPage }                 from '../pages/devices/devices';
import { ReportPage }                  from '../pages/reports/report/report';
import { AddPage }                     from '../pages/reports/add/add';
import { ChangepasswordPage }          from '../pages/changepassword/changepassword';

import { StatusBar }                   from '@ionic-native/status-bar';
import { SplashScreen }                from '@ionic-native/splash-screen';
import { AnnouncementServiceProvider } from '../providers/announcement-service/announcement-service';
import { ReportServiceProvider }       from '../providers/report-service/report-service';
import { LoginServiceProvider }        from '../providers/login-service/login-service';
import { UserServiceProvider }         from '../providers/user-service/user-service';
import { DeviceServiceProvider }       from '../providers/device-service/device-service';

import { MenuComponent }               from "../components/menu/menu.component";
import { LoginModule }                 from "../pages/login/login.module";
import { InAppBrowser }                from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AnnouncementsPage,
    DevicesPage,
    ReportsPage,
    RegistrationPage,
    AnnouncementPage,
    ReportPage,
    AddPage,
    MenuComponent,
    ChangepasswordPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    LoginModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AnnouncementsPage,
    DevicesPage,
    RegistrationPage,
    ReportsPage,
    AnnouncementPage,
    ReportPage,
    AddPage,
    MenuComponent,
    ChangepasswordPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AnnouncementServiceProvider,
    ReportServiceProvider,
    LoginServiceProvider,
    UserServiceProvider,
    DeviceServiceProvider
  ]
})
export class AppModule {}
